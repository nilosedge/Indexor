

#if !defined(HASHTABLE_H)
#define HASHTABLE_H

#include "Hasher.h"
#include "BinaryTree.h"

namespace Hashing { 

	template
		<
			class Value			= int,
//			const Value EmptyValue	= -1,
			class Key			= String,
			class Hasher		= StringHasher,
			void (*Deleter)(Value) = ValueDeleter,
			class Bucket		= BinaryTree<Value,Key,Hasher>
		>
		class HashTable {
			Bucket** buckets;
			int  bucketCount;

		public:

			HashTable(int initialSize) {
				bucketCount = Hasher::GetInitialSize(initialSize);
				buckets = new Bucket*[bucketCount];

				for (int index = 0; index < bucketCount; index++) {
					buckets[index] = NULL;
				}
			}

			~HashTable() {
				for (int index = 0; index < bucketCount; index++) {
					Bucket* temp = buckets[index];
					if (temp) delete temp;
				} if(buckets) { delete buckets; }
			}

			inline void Add(Key key, Value value) {
				unsigned int bucketHandle;
				bucketHandle = Hasher::Hash(key) % bucketCount;
				if (buckets[bucketHandle]) buckets[bucketHandle]->Add(key, value);
				else buckets[bucketHandle] = new Bucket(key, value);
			}

			void Remove(Key key) {
				unsigned int bucketHandle = Hasher::Hash(key) % bucketCount;
				if (!buckets[bucketHandle]) return;
				if (buckets[bucketHandle]->Remove(key)) buckets[bucketHandle] = NULL;
			}

			inline bool Lookup(Key key, Value* pvalue) {
				int bucketHandle = Hasher::Hash(key) % bucketCount;
				if (buckets[bucketHandle] == NULL) return false;
				return buckets[bucketHandle]->Lookup(key, pvalue);
			}

			inline void PrintKeys(ofstream *out) {
				for (int index = 0; index < bucketCount; index++) {
					if (buckets[index]) buckets[index]->PrintKeys(out);
				}				
			}
		};
}


#endif // HASHTABLE_H
