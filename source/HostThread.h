#ifndef HostThread_h_
#define HostThread_h_

#include "PracticalSocket.h"
#include "libsmbclient.h"
#include "Queue.h"
#include "DataNode.h"
#include <iostream>
#include <cc++2/cc++/thread.h>
#include <mysql/mysql.h>
#include "func_db.h"
#include "HostInfo.h"
#include "HashTable.h"
#include "DBThread.h"

	using namespace ost;
	using namespace std;

	class HostThread : public ost::Thread {
		public:
			HostThread(string ip, int HostId, int Rel, DBThread *dbthread, HostInfo *info, Hashing::HashTable<unsigned long long> *words);
			~HostThread() {
				if(fileurl) delete fileurl;
				if(sqlcommand) delete sqlcommand;
			};
			int GetFileList(char *url);
			void Strip(char *s);
			bool isFinished();
			virtual void run();
			volatile int HostId;

		protected:
			string ip;
			DBThread *dbthread;
			volatile int con;
			volatile int Rel;
			char *fileurl;
			char *sqlcommand;
			MYSQL_ROW row;
			Hashing::HashTable<unsigned long long> *words;
			HostInfo *info;

		private:
			bool debug;
			volatile bool finished;
			void DoKeywords(char *next, int FileId);

	};

#endif
