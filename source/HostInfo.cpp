#include "HostInfo.h"
#include "PracticalSocket.h"
#include <iostream>
#include "libsmbclient.h"
#include <mysql/mysql.h>
#include "func_db.h"
#include <netdb.h>
#include <sys/socket.h>
#include "Queue.h"
#include <unistd.h>
#include <fstream>
#include "HashTable.h"
#include <libping/ping.h>


using namespace std;

static void auth_fn (const char *srv, const char *shr, char *wg, int wglen, char *un, int unlen, char *pw, int pwlen) {
	strcpy(un, "Guest");
	strcpy(pw, ""); 
} 

HostInfo::HostInfo() {
	sema = new ost::Semaphore(1);
	if (smbc_init(&auth_fn, 0) < 0) { exit(0); }
	debug = false;
}

char * HostInfo::GetMachineName(string Ip) {

	sema->wait();

	char *machine = new char[50];

	if(pinghost(Ip.c_str()) == 1) {

		unsigned short port = 137;
		int trys = 10;
		char buffer[50];
		char bigBuffer[500];
		UDPSocket udp;
		//hostent *host = gethostbyname(ip);
		//unsigned long IP = ntohl(*((unsigned long *)host->h_addr_list[0]));
		if(debug) cout << "Addr: " << Ip << endl;
		try {
			udp.connect(Ip, port);
		} catch (SocketException s) {
			cout << s.GetMessage();
			exit(0);
		}

		strcpy(buffer, "             CKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA  !  ");
		buffer[0] = 0x51;
		buffer[1] = 0x75;
		buffer[2] = 0x00;
		buffer[3] = 0x00;
		buffer[4] = 0x00;
		buffer[5] = 0x01;
		buffer[6] = 0x00;
		buffer[7] = 0x00;
		buffer[8] = 0x00;
		buffer[9] = 0x00;
		buffer[10] = 0x00;
		buffer[11] = 0x00;
		buffer[45] = 0x00;
		buffer[46] = 0x00;
		buffer[48] = 0x00;
		buffer[49] = 0x01;
		udp.send(buffer, 50);
	
		int in = 0;
		int try_count = 0;
		while((in = recv(udp.sockDesc, bigBuffer, 500, MSG_DONTWAIT)) == -1 && try_count < trys) {
			usleep(100000);
			try_count++;
		}
		strcpy(machine, bigBuffer + 57);
		//machine = bigBuffer + 57;
		udp.disconnect();
		if(in < 0 || strlen(machine) == 0) machine = NULL;

	} else {
		delete machine;
		machine = NULL;
	}	
	sema->post();
	return machine;
}
