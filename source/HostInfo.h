#ifndef HostInfo_h_
#define HostInfo_h_

#include <fstream>
#include <mysql/mysql.h>
#include <cc++2/cc++/thread.h>
#include "func_db.h"
#include "HashTable.h"


	using namespace std;

	class HostInfo {

		public:
			HostInfo();
			~HostInfo() { delete db; delete sema; }
			char * GetMachineName(string Ip);
		protected:
			Hashing::HashTable<unsigned long long> *words;
			func_db *db;
			ost::Semaphore *sema;

		private:
			bool debug;

	};

#endif
