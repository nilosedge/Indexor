#ifndef Node_h_
#define Node_h_

//#include "HostThread.h"
#include <iostream>

	using namespace std;

	class Node {
		public:

			Node(int id = 0);
			//Node(char *_worddata); 
			//Node(unsigned long long data);
			//Node(unsigned long long data, char *worddata);
			//Node(HostThread *thread);
			virtual void Print() = 0;
			virtual ~Node();

		public:
			//unsigned long long data;
			int id;
			Node *Next, *Prev;
			//bool check;
			//char *worddata;
			//HostThread *thread;
			//MYSQL_RES *result;

	};

#endif
