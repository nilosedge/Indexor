#include <iostream>
#include "Queue.h"
#include <mysql/mysql.h>
#include "func_db.h"
#include <string>

#include "PracticalSocket.h"
#include "HostInfo.h"
#include "HostThread.h"
#include "ThreadNode.h"
#include "QueryNode.h"
#include "DBThread.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <libping/ping.h>


using namespace std;


int main() {
	MYSQL_ROW row;
	MYSQL_ROW row2;

	char *sqlcommand = new char[1000];
	string ip;

	HostInfo *info = new HostInfo();
	Queue *queue = new Queue(100000);

	Hashing::HashTable<unsigned long long> *words = new Hashing::HashTable<unsigned long long>(10);

	DBThread *dbthread = new DBThread();
	dbthread->start();
	sleep(1);
	cout << "Waiting for DB Thread to start." << endl;
	sleep(1);



	sprintf(sqlcommand, "select * from Skipwords");
	//cout << sqlcommand << endl;
	QueryNode *q = new QueryNode(sqlcommand, SELECT_QUERY, false);
	dbthread->Add(q);

	while(q->Completed == false) { usleep(10); }

	if(q->result) { 
		while((row = dbthread->db->get_row(q->result)) != NULL) { 
			//cout << "Adding: " << row[0] << endl;
			words->Add(row[0], 0); 
		}
	}
	delete q;



	sprintf(sqlcommand, "select * from Hosts");
	//cout << sqlcommand << endl;
	q = new QueryNode(sqlcommand, SELECT_QUERY, false);
	dbthread->Add(q);
	while(q->Completed == false) { usleep(10); }


	if(q->result) {
		if(dbthread->db->num_rows(q->result) > 0) {
			while((row = dbthread->db->get_row(q->result)) != NULL) {
				//HostThread *th = new HostThread(ip, atoi(row[0]), atoi(row[4]), db, info);
				queue->Push(new ThreadNode(new HostThread(row[4], atoi(row[0]), atoi(row[3]), dbthread, info, words)));
				//node->thread->start();

			}
		} else {
			cout << "No hosts found." << endl;
			exit(0);
		}
	} else {
		cout << "No result.\n";
		exit(0);
	}
	delete q;



	for(ThreadNode *T = (ThreadNode *)queue->Head; T != NULL; T = (ThreadNode *)T->Next) {
		T->thread->start();
		sleep(1);
	}

	bool finished = false;

	cout << "Waiting for nodes to finish" << endl;

	while(!finished) {

		ThreadNode *T = (ThreadNode *)queue->Pop();
		if(T->thread->isFinished()) { 
			//cout << "We are deleting T\n";
			delete T;
		} else {
			queue->Push(T);
			usleep(10);
		}

		if(queue->CurrentSize == 0) { finished = true; }
	}

	cout << "Finished scanning the nodes." << endl;


	cout << "Waiting for insert queries to finish." << endl;

	while(dbthread->QueryQueue->CurrentSize > 0) {
		cout << "Querys left: " << dbthread->QueryQueue->CurrentSize << endl;
		sleep(2);
	}

	cout << "Insert queries finished." << endl;



//		while(Node *T = queue->Pop()) {
//			cout << "Starting " << T->thread->HostId << endl;
//			T->thread->start();
//			cout << "Ending " << T->thread->HostId << endl;
//			delete T;
//		}


	cout << "Waiting for clean up queries to finish." << endl;

	sprintf(sqlcommand, "select * from Hosts where Available=0 and Reliability<=0");
	q = new QueryNode(sqlcommand, SELECT_QUERY, false);
	dbthread->Add(q);
	while(!q->Completed) { usleep(10); }

	if(q->result) {

		while((row = dbthread->db->get_row(q->result))) {
			sprintf(sqlcommand, "select id from Files where HostId=%i", atoi(row[0]));
			QueryNode *working = new QueryNode(sqlcommand, SELECT_QUERY, false);
			dbthread->Add(working);
			while(!working->Completed) { usleep(10); }

			if(working->result) {
				while((row2 = dbthread->db->get_row(working->result))) {
					sprintf(sqlcommand, "delete from Keywords where FileId=%i", atoi(row2[0]));
					dbthread->Add(new QueryNode(sqlcommand, DELETE_QUERY, true));
					sprintf(sqlcommand, "delete from Files where Id=%i", atoi(row2[0]));
					dbthread->Add(new QueryNode(sqlcommand, DELETE_QUERY, true));
				}
			}
			delete working;


		}
	}
	delete q;

	while(dbthread->QueryQueue->CurrentSize > 0) {
		cout << "Querys left: " << dbthread->QueryQueue->CurrentSize << endl;
		sleep(2);
	}

	cout << "Clean up queries are finished." << endl;
	//delete dbthread;
}
