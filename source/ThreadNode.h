#ifndef ThreadNode_h_
#define ThreadNode_h_

#include "Node.h"
//#include <cc++2/cc++/thread.h>
#include "HostThread.h"
#include <iostream>

	using namespace std;
	using namespace ost;

	class ThreadNode : public Node {

		public:
			ThreadNode(HostThread *_thread) : Node(), thread(_thread), finished(false) {};
			void Print();
			~ThreadNode() { delete thread; };
			HostThread *thread;
			volatile bool finished;

	};

#endif
