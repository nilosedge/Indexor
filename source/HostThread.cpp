#include "HostThread.h"


static void auth_fn (const char *srv, const char *shr, char *wg, int wglen, char *un, int unlen, char *pw, int pwlen) {
	strcpy(un, "Guest");
	strcpy(pw, ""); 
} 

HostThread::HostThread(string _ip, int _HostId, int _Rel, DBThread *_dbthread, HostInfo *_info, Hashing::HashTable<unsigned long long> *_words) {
	HostId = _HostId;
	ip = _ip;
	Rel = _Rel;
	dbthread = _dbthread;
	info = _info;
	debug = false;

	fileurl = new char[30];
	sqlcommand = new char[10000];
	words = _words;
	finished = false;

	if (smbc_init(&auth_fn, 0) < 0) { exit(); }
}


void HostThread::run() {

	cout << "Host: " << ip << " is starting." << endl;

	char *machine = info->GetMachineName(ip);

	if(machine && strlen(machine) > 0) {

		QueryNode *q;

		cout << ip << ": " << " HostId: " << HostId << " has name: " << machine << endl;

		for(int i = strlen(machine) - 1; machine[i] == ' '; i--) {
			machine[i] = 0;
		}
		Rel++;
		if(Rel < 0) Rel = 0;
		if(Rel > 10) Rel = 10;

		sprintf(sqlcommand, "update Hosts set Name=\"%s\", Available=1, Reliability=%i where Id=%i", machine, Rel, HostId);
		if(debug) cout << sqlcommand << endl;

		q = new QueryNode(sqlcommand, UPDATE_QUERY, false);
		dbthread->Add(q);
		while(q->Completed == false) { usleep(10); }
		delete q;
		
		delete machine;

		sprintf(fileurl, "smb://%s", ip.c_str());

		if(debug) cout << "Host Id " << HostId << endl;
		sprintf(sqlcommand, "select now()+%i", 0);

		q = new QueryNode(sqlcommand, SELECT_QUERY, false);
		dbthread->Add(q);
		while(q->Completed == false) { usleep(1); }

		row = dbthread->db->get_row(q->result);

		char *start = new char[strlen(row[0])+1];
		strcpy(start, row[0]);

		delete q;

		GetFileList(fileurl);
		
		sprintf(sqlcommand, "select * from Files where HostId=%i and Updated < (%s - (60 * 5))", HostId, start);
		q = new QueryNode(sqlcommand, SELECT_QUERY, false);
		dbthread->Add(q);
		while(q->Completed == false) { usleep(1); }

		delete start;
		if(q->result) {
			while((row = dbthread->db->get_row(q->result))) {
				sprintf(sqlcommand, "delete from Files where Id=%i", atoi(row[0]));
				dbthread->Add(new QueryNode(sqlcommand, DELETE_QUERY, true));
				sprintf(sqlcommand, "delete from Keywords where FileId=%i", atoi(row[0]));
				dbthread->Add(new QueryNode(sqlcommand, DELETE_QUERY, true));
			}
		}
		delete q;

	} else {

		Rel-=3;
		if(Rel < 0) Rel = 0;
		if(Rel > 10) Rel = 10;

		sprintf(sqlcommand, "update Hosts set Available=0, Reliability=%i where Id=%i", Rel, HostId);
		dbthread->Add(new QueryNode(sqlcommand, UPDATE_QUERY, true));
		//cout << db->get_error() << endl;
	}
	cout << "Host: " << ip << " is finished." << endl;
	finished = true;
	return;
}

int HostThread::GetFileList(char *url) {

	struct smbc_dirent *de;
	struct stat st;
	int dh;
	int dh2;
	int ret = 1;
	char *next;
	char path[10000];
	char filename[2000];
	unsigned long long FileId;

	//cout << "Are we opening " << url << endl;
	if ((dh = smbc_opendir(url)) < 0 ) { ret = 0; }

	while((de = smbc_readdir(dh)) && ret == 1) {

		next = new char[10000];
		strcpy(next, url);
		strcat(next, "/");
		strcpy(filename, de->name);
		strcpy(path, next);
		strcat(next, filename);
		//if (url[strlen(url)]!='/')strcat(next, "/");


		switch(de->smbc_type) { 
			case SMBC_DIR:
			case SMBC_FILE_SHARE:

				if (strcmp(filename,".") && strcmp(filename, "..") ) {
					//cout << "NextD: -->" << next << "<--" << endl;
					ret = GetFileList(next);
				}
				break;
			case SMBC_FILE:
				dh2 = dh;
				smbc_stat(next, &st);
				dh = dh2;

				sprintf(sqlcommand, "select * from Files where FilePath=\"%s\" and FileName=\"%s\" and Size=%i and HostId=%i", path, filename, (int)st.st_size, HostId);

				QueryNode q(sqlcommand, SELECT_QUERY, false);
				dbthread->Add(&q);
				while(q.Completed == false) { usleep(1); }

				if(q.result && dbthread->db->num_rows(q.result) > 0) {
					row = dbthread->db->get_row(q.result);

					int rel = atoi(row[6]) + 1;
					if(rel < 0) rel = 0;
					if(rel > 10) rel = 10;

					FileId=atol(row[0]);
					if(!strcmp(path, row[2])) {
						sprintf(sqlcommand, "update Files set Reliability=%i, Updated=now() where Id=%i", rel, (int)FileId);
						dbthread->Add(new QueryNode(sqlcommand, UPDATE_QUERY, true));
					} else {
						sprintf(sqlcommand, "update Files set Reliability=1, FilePath=\"%s\" where Id=%i", path, (int)FileId);
						dbthread->Add(new QueryNode(sqlcommand, UPDATE_QUERY, true));
						sprintf(sqlcommand, "delete from Keywords where FileId=%i", (int)FileId);
						dbthread->Add(new QueryNode(sqlcommand, DELETE_QUERY, true));
						DoKeywords(next, FileId);
					}
				} else {
					sprintf(sqlcommand, "insert into Files (HostId, FilePath, FileName, Size, Reliability) values(%i, \"%s\", \"%s\", %i, 1)", HostId, path, filename, (int)st.st_size);
					QueryNode q2(sqlcommand, INSERT_QUERY, false);
					dbthread->Add(&q2);
					while(q2.Completed == false) { usleep(1); }
					FileId = q2.Index;
					DoKeywords(next, FileId);
				}
				break;
		}

		delete next;
	}
	smbc_closedir(dh);
	return ret;
} 

void HostThread::DoKeywords(char *next, int FileId) {

	char *word;
	char *data;
	char line[1000];

	Queue *wordqueue = new Queue(1000);
	Hashing::HashTable<unsigned long long> *foo = new Hashing::HashTable<unsigned long long>(100000);


	char *pointer;
	for(pointer = next + 6; pointer[0] != '/'; pointer++) { }
	pointer++;
	char *working = new char[10000];
	strcpy(working, pointer);
	Strip(working);

	for(word = strtok(working, " "); word != NULL; word = strtok(NULL, " ")) {
		if(!words->Lookup(word, 0)) {
			if(!foo->Lookup(word, 0)) {
				data = new char[strlen(word)+1];
				strcpy(data, word);
				wordqueue->Push(new DataNode(FileId, data));
				foo->Add(data, 0);
			}
		}
	}
	delete working;

	while(wordqueue->CurrentSize > 0) {

		for(DataNode *T = (DataNode *)wordqueue->Head; T != NULL; T = (DataNode *)T->Next) {

			strcpy(line, T->worddata);
			for(DataNode *W = (DataNode *)T->Next; W != NULL; W = (DataNode *)W->Next) {
				strcat(line, " ");
				strcat(line, W->worddata);
			}
			sprintf(sqlcommand, "insert into Keywords (Keyword, FileId) values(\"%s\", %i)", line, (int)FileId);
			dbthread->Add(new QueryNode(sqlcommand, 0, true));
		}
		Node *N = wordqueue->Pop();
		delete N;
	}

	delete wordqueue;
	delete foo;
}

void HostThread::Strip(char * s) {
	if(s) {
		int i = 0;
		while(s[i] != '\0') {
			if(s[i] >= 65 && s[i] <= 90) { s[i] = tolower(s[i]); }
			else if(!((s[i] >= 48 && s[i] <= 57) || (s[i] >= 97 && s[i] <= 122))) s[i] = ' ';
			//if(!((s[i] >= 48 && s[i] <= 57) || (s[i] >= 65 && s[i] <= 90) || (s[i] >= 97 && s[i] <= 122))) s[i] = ' ';
			i++;
		}
	}
}


bool HostThread::isFinished() {
	return finished;
}
