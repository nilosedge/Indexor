<?php
	include("config.php");
	printheader("Documentation");
?>

	<center>
	<h1>Install / Documentation  2.0.0</h1>

	<table border=1>
		<tr>
			<td>Step 1: Become ROOT.</td>
			<td>Otherwise none of the following steps will work.</td>
		</tr>
		<tr>
			<td>Step 2: Dependencies</td>
			<td>There are a bunch of libraries that you need to install in order to get this thing to work.<BR>
					The following is a list of packages you need installed, this is what they are called in Debian. So which ever linux version you are running you will need to figure out what the equivalant package is and install it.<BR>
				<pre>
		libmysqlclient10-dev
		libmysqlclient10
		libmysqlclient12
		mysql-client
		mysql-common
		mysql-server
		apache
		php4-mysql
		php4
		libsmbclient
		libsmbclient-dev
		libcommoncpp2-dev
		libcommoncpp2-1.3c2
		libping (comes with nilondex)
				</pre>
			</td>
		</tr>
		<tr>
			<td width=25%>Step 3: Download Nilondex.</td>
			<td>This includes the Websource and the backend Nilondex. Untar the file anywhere you want. The directory structure is as follows:<BR>
				nilondex/source/websource/webroot<BR>
				nilondex/source/websource<BR>
				nilondex/source/libping-1.15<BR>
				nilondex/source/sql<BR>
				The web source can be copied anywhere thats in the webroot of apache.<BR><BR>
				Under the sql dir is the file you will want to import into your mysql database.<BR>
				This will create a new database IndeXor2 just incase you had installed the first version.<BR>


			</td>
		</tr>
		<tr>
			<td>Step 4: Compling</td>
			<td>

				Compling Nilondex. Untar the Nilondex.tar.gz anywhere you want. It will create a directory called nilondex/source. This is the working direcotry. 
				Before Compiling Nilondex you need to add a custom library (libping). 
				In the nilondex/source directory there is a directory called "libping-1.15" go into that dir and type the following commands:
				<pre>
	prompt>./configure
	prompt>make
	prompt>make install
				</pre>
					This will install libping. The installer skips the header files so those will need to be copied by hand. Enter the following commands from the libping dir:
				<pre>
	mkdir /usr/local/include/libping
	cp include/*.h /usr/local/include/libping
				</pre>
					Run the following commands to make sure libping is installed:
				<pre>
	prompt>ldconfig -p | grep ping
        libping.so.1 (libc6) => /usr/local/lib/libping.so.1
        libping.so (libc6) => /usr/local/lib/libping.so
				</pre>
						Which means that your libping is installed correctly.<BR><BR>
						<b>DON'T SKIP</b> you need to edit the DBThread.cpp and at the top of the file change the username and password values for your database. These should also be the same things used in the config.php that comes with the web source. The following lines need to be changed:
<pre>
   if(!((con = db->connect("localhost", "IndeXor", "dbpass")) > 0)) {
      cout << "Could not connect to db.\n";
      exit();
   }
   db->use_database("IndeXor");
</pre>
Localhost is the host the DB server is on.<BR>
IndeXor is the username nilondex is going to connect as.<BR>
dbpass is the password for user IndeXor<BR>
And under use_database change that to the name of the database you created, if you used the script in the sql directory then this needs to be IndeXor2.<BR>
<BR>
<BR>

						Go into the nilondex/source dir and type "make". If you have everything installed that you need, it will compile everything (no NO errors or warnings) and create a binary called: browser. You should see the following output from the "make" command:
				<pre>
prompt>make clean
prompt>make 
c++ -g -pg -Wall -I/usr/include/cc++2 -c HostInfo.cpp
c++ -g -pg -Wall -I/usr/include/cc++2 -c PracticalSocket.cpp
c++ -g -pg -Wall -I/usr/include/cc++2 -c Queue.cpp
c++ -g -pg -Wall -I/usr/include/cc++2 -c Node.cpp
c++ -g -pg -Wall -I/usr/include/cc++2 -c func_db.cpp
c++ -g -pg -Wall -I/usr/include/cc++2 -c QueryNode.cpp
c++ -g -pg -Wall -I/usr/include/cc++2 -c HostThread.cpp
c++ -g -pg -Wall -I/usr/include/cc++2 -c ThreadNode.cpp
c++ -g -pg -Wall -I/usr/include/cc++2 -c DataNode.cpp
c++ -g -pg -Wall -I/usr/include/cc++2 -c DBThread.cpp
c++ -g -pg -Wall  -o browser -lnsl -lresolv -lsmbclient -lmysqlclient_r -lccgnu2 -lpthread -ldl
-lping HostInfo.o PracticalSocket.o Queue.o Node.o func_db.o QueryNode.o HostThread.o ThreadNode.o
DataNode.o DBThread.o -I/usr/include/cc++2 browser.cpp
prompt>
				</pre>
					If you get errors or warnings then you are missing something.
			</td>
		</tr>
		<tr>
			<td>Step 5: Mysql Database Setup</td>
			<td>You need to setup a database. You also need to setup a user that can access the database. You need to edit the config.php in the websource dir to have the SAME database name, username, password.<BR>
				You can insert the database Schema file by the following command:
				<pre>
	prompt>mysql dbname -u root -p < sql/IndeXor2.sql
	Password:
	prompt>
				</pre>
				At the prompt enter your root users database password. Also dbname is going to be the name of the database you plan to use for the IndeXor.
			</td>
		</tr>
		<tr>
			<td>Step 6: Running the IndeXor</td>
			<td>Running the IndeXor is now really easy.
				<pre>
	./browser
				</pre>
					Note browser might take a while, I have not run the new one on the dorm.<BR>
			</td>
		</tr>
		<tr>
			<td>Step 7: It WORKS!!!!</td>
			<td>I will still give anyone a dollor to get this thing working on your first try, with two exceptions Moof or Striker sorry I know you two too well.<BR>
				I am sure there is going to be questions. Just email me or post to the thread on the board, and I will try to help the best I can.<BR><BR>NOTE: This is version 2.0 I will NOT make revisions to 1.0 its DEAD ive learned much and don't plan to support 1.x.<BR>
				NOTE2: THIS ONLY WORKS ON LINUX. IF YOU GET HERE AND HAVEN'T FIGURED THAT OUT YET, I AM SORRY FOR YOU.
				Note3: I have no plans to porting this to windows. The reason why I wrote it on linux in the first place. I wanted to compete with the person that origanally wrote it for windows and it took 5 hours to index. My shortest indexing time was 7 minutes. Linux... Windows... I choose LINUX!
			</td>
		</tr>

		<tr>
			<td>Troubles:</td>
			<td>
				Troubles: If you are experiencing the error Neighbor Table Overflow your arp cache is to small for the network the fix is add the following line to your /etc/sysctl.conf file:<BR>
				<BR>
				net.ipv4.neigh.default.gc_thresh3 = 2048<BR>
				<BR>
				once you do this to load it into your kernel type<BR>
				<BR><pre>
				prompt>sysctl -p
				net.ipv4.neigh.default.gc_thresh3 = 2048
				prompt></pre>
				<BR>
				Other simptoms: None that I know of.
				<BR><BR>
				Also if you notice that your ./browser program does NOTHING. Chances are the smbc_init function is looking for a smb.conf file. You can fix this by creating an empty file in /root/.smb/smb.conf and or /etc/samba/smb.conf
				<pre>
				prompt>mkdir /root/.smb
				prompt>touch /root/.smb/smb.conf
				prompt>touch /etc/samba/smb.conf
				</pre>
			</td>
		</tr>

		<tr>
			<td>Step 8: Its not scaning ANYONE!!!!</td>
			<td>Its been turned into a opt in scanner. On the web page there are links for people to be added or removed. It will be up to them to
				come and click remove me from this site. If you noticed in the Hosts table in the database you can add all the ip address' you want.
				Just put in all the address' you want to scan and it will try to scan them. It now has become more internet friendly as in you can
				scan anyone, not just the dorm.
			</td>
		</tr>


	</table>

	</center>





<?php 
	printfooter("short");
?>
