<?php

	include("config.php");
	include("func_db_mysql.php");

	$db = new Database($DEBUG);
	$db->connect($INDEXOR_DBHOST, $INDEXOR_DBUSER, $INDEXOR_DBPASSWD);
	$db->usedatabase($INDEXOR_DBNAME);

	print "<pre>\n";
	$res = $db->query("select * from hosts");
	while($a = $db->get_array($res)) {
		print "\t<host id=$a[id] name=\"$a[name]\" ip=\"$a[ip]\">\n";
		$res2 = $db->query("select * from shares where host_id=$a[id]");
			while($a2 = $db->get_array($res2)) {
				print "\t\t<share Name=\"$a2[name]\">\n";
				$res3 = $db->query("select * from files where share_id=$a2[id]");
				while($a3 = $db->get_array($res3)) {
					print "\t\t\t<file id=$a3[id] FilePath=\"$a3[file_path]\" FileName=\"$a3[file_name]\" Size=$a3[size] Extention=\"$a3[extention]\"></file>\n";
				}
				print "\t\t</share>\n";
			}
		print "\t</host>\n";
	}
	print "</pre>\n";

?>
