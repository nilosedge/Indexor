<?php

global $FUNC_INDEXOR_TBL_HOSTS_PHP;
if (!$FUNC_INDEXOR_TBL_HOSTS_PHP) {
   $FUNC_INDEXOR_TBL_HOSTS_PHP=1;

	include("func_db_mysql.php");

   class HOSTS {

      var $db, $resultid;
      var $table, $debug;

      function HOSTS($debug) {
			global $INDEXOR_DBUSER, $INDEXOR_DBNAME, $INDEXOR_DBHOST, $INDEXOR_DBPASSWD;

			$this->debug = $debug;
         $this->table = "Hosts";
         $this->db = new Database($debug);
			$this->db->connect($INDEXOR_DBHOST, $INDEXOR_DBUSER, $INDEXOR_DBPASSWD);
			$this->db->usedatabase($INDEXOR_DBNAME);
      }

		function get_info($id) {
			return $this->db->get_array($this->db->query("select * from $this->table where Id=$id"));
		}

		function host_exists($ip) {
			$array = $this->db->get_array($this->db->query("select count(*) as count from $this->table where Ip = \"$ip\""));
			return $array[count];
		}

		function add_host($ip) {
			if(!$this->host_exists($ip)) {
				$this->db->query("insert into $this->table (Ip) values(\"$ip\")");
				return true;
			}
			return false;
		}

		function remove_host($ip) {
			$this->db->query("delete from $this->table where Ip = \"$ip\"");
		}
	
	} // end class HOSTS

} // end include protection

?>
