<?php
	include("config.php");
	printheader("Documentation");
?>

	<center>
	<h1>Install / Documentation  1.00</h1>

	<table border=1>
		<tr>
			<td>Step 1: Become ROOT.</td>
			<td>Otherwise none of the following steps will work.</td>
		</tr>
		<tr>
			<td>Step 2: Dependencies</td>
			<td>There are a bunch of libraries that you need to install in order to get this thing to work.<BR>
					The following is a list of packages you need installed, this is what they are called in Debian. So which ever linux version you are running you will need to figure out what the equivalant package is and install it.<BR>
				<pre>
		libmysqlclient10-dev
		libmysqlclient10
		libmysqlclient12
		mysql-client
		mysql-common
		mysql-server
		apache
		php4-mysql
		php4
		smbfs
		smbclient
		libsmbclient
		libsmbclient-dev
		samba
		samba-common
		smb++ (comes with nilondex)
				</pre>
			</td>
		</tr>
		<tr>
			<td width=25%>Step 3: Download Everything.</td>
			<td>This includes the "<a href="http://mp3s.nilosplace.net/IndeXor/IndeXor.tar.gz">Websource</a>" and the backend "<a href="http://mp3s.nilosplace.net/IndeXor/Nilondex.tar.gz">Nilondex</a>". Untar the web source in a directory/folder that is in the webroot of your web server. You can untar by using the following command: <pre>prompt>tar zxvf IndeXor.tar.gz</pre><BR>When you extract the Websource it will create a directory called IndeXor which will be where the base of the site will run from.</td>
		</tr>
		<tr>
			<td>Step 4: Compling</td>
			<td>

				Compling the IndeXor (Nilondex). Untar the Nilondex.tar.gz anywhere you want. It will create a directory called nilondex. This is the working direcotry. 
				Before Compiling Nilondex you need to add a custom library (smb++). 
				In the nilondex directory there is a directory called "libsmb++-0.9.1" go into that dir and type the following commands:
				<pre>
	prompt>make clean
	prompt>make
	prompt>make install
				</pre>
					This will install smb++. To verify that it is installed correctly run the following command:
				<pre>
	prompt>ldconfig -p | grep smb
				</pre>
					You should see the following:
				<pre>
        libsmbclient.so.0 (libc6) => /usr/lib/libsmbclient.so.0
        libsmbclient.so (libc6) => /usr/lib/libsmbclient.so
        libsmb++.so.0 (libc6) => /usr/local/lib/libsmb++.so.0
        libsmb++.so (libc6) => /usr/local/lib/libsmb++.so
				</pre>
						Which means that your smbclient is installed correctly and the smb++ is installed correctly.<BR><BR>
					In the nilondex directory is the source directory. Go into that directory and type "make". If you have everything installed that you need, it will compile everything (no NO errors or warnings) and create a binary called: browser. You should see the following output from the "make" command:
				<pre>
prompt>make clean
prompt>make install
c++ -g -pg -Wall  -c PracticalSocket.cpp
c++ -g -pg -Wall  -c HostInfo.cpp
c++ -g -pg -Wall browser.cpp -o browser -lsmb++ -lnsl -lresolv -lsmbclient HostInfo.o PracticalSocket.o
strip browser
c++ -g -pg -Wall  -c func_db.cpp
c++ -g -pg -Wall  -o genkeys -lmysqlclient func_db.o gen_keywords.cpp
strip genkeys
c++ -g -pg -Wall  lines.cpp -o lines
strip lines
c++ -g -pg -Wall  getname.cpp -o getname PracticalSocket.o HostInfo.o -lsmb++ -lnsl -lresolv -lsmbclient
strip getname
cp browser ..
cp genkeys ..
cp getname ..
cp lines ..
prompt>
				</pre>
					If you get errors or warnings then you are missing something.
			</td>
		</tr>
		<tr>
			<td>Step 5: Mysql Database Setup</td>
			<td>You need to setup a database. You also need to setup a user that can access the database. You need to edit the addfiles.pl script and put the database name, username, and password that is going to be used. You also need to edit the config.php in the websource dir to have the SAME database name, username, password.<BR>
				I will be creating a database script that does this for you. It will also setup the tables that the database will use.<BR>
				Ok the database Schema file is out, you can get it <a href="http://mp3s.nilosplace.net/IndeXor/IndeXor.sql">here</a>. You can insert the tables via the following command:
				<pre>
	prompt>mysql dbname -u root -p < IndeXor.sql
	Password:
	prompt>
				</pre>
				At the prompt enter your root users database password. Also dbname is going to be the name of the database you plan to use for the IndeXor.
			</td>
		</tr>
		<tr>
			<td>Step 6: Running the IndeXor</td>
			<td>The run order is from the nilondex dir
				<pre>
	./browser
	cat shares/* > dorm-filelist
	./genkeys
	./addfiles.pl
				</pre>
					Note browser in its current state takes like 30 minutes to run.<BR>
					Genkeys will take up to 200M RAM.
					Addfiles.pl can take up to an hour to run depending on your CPU speed, plus causing Mysql to take 500M+ of RAM.
			</td>
		</tr>
		<tr>
			<td>Step 7: It WORKS!!!!</td>
			<td>I will give anyone a dollor to get this thing working on your first try, with two exceptions Moof or Striker sorry I know you two too well.<BR>
				I am sure there is going to be questions. Just email me or post to the thread on the board, and I will try to help the best I can.<BR><BR>NOTE: This is version 1.0 I am working on 2.0 but I will make revisions to 1.0 as need be in order to get things working.<BR>
				NOTE2: THIS ONLY WORKS ON LINUX. IF YOU GET HERE AND HAVEN'T FIGURED THAT OUT YET, I AM SORRY FOR YOU.
				Note3: I have no plans to porting this to windows. The reason why I wrote it on linux in the first place. I wanted to compete with the person that origanally wrote it for windows and it took 5 hours to index. My shortest indexing time was 7 minutes. Linux... Windows... I choose LINUX!
			</td>
		</tr>

		<tr>
			<td>Troubles:</td>
			<td>
				Troubles: If you are experiencing the error Neighbor Table Overflow your arp cache is to small for the network the fix is add the following line to your /etc/sysctl.conf file:<BR>
				<BR>
				net.ipv4.neigh.default.gc_thresh3 = 2048<BR>
				<BR>
				once you do this to load it into your kernel type<BR>
				<BR><pre>
				prompt>sysctl -p
				net.ipv4.neigh.default.gc_thresh3 = 2048
				prompt></pre>
				<BR>
				Other simptoms are if it gets like half way through the ip list and then just stops for like a whole minute or two.
				The last IP is 216.229.239.254 browser will sit there for a while still trying to connect to the people that it missed along the way it will stop after a minute or so, if not you can just hit ctrl-c.
				<BR><BR>
				Also if you notice that your ./browser program does NOTHING. Chances are the smbc_init function is looking for a smb.conf file. You can fix this by creating an empty file in /root/.smb/smb.conf and or /etc/samba/smb.conf
				<pre>
				prompt>mkdir /root/.smb
				prompt>touch /root/.smb/smb.conf
				prompt>touch /etc/samba/smb.conf
				</pre>
			</td>
		</tr>

	</table>

	</center>





<?php 
	printfooter("short");
?>
