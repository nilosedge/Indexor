<?php

global $FUNC_INDEXOR_TBL_INDEXORLOG_INC;
if (!$FUNC_INDEXOR_TBL_INDEXORLOG_INC) {
	$FUNC_INDEXOR_TBL_INDEXORLOG_INC=1;

	include("func_db_mysql.php");

	class IndeXorLog {

		var $db, $resultid;
		var $table, $debug;

		function IndeXorLog($debug){
			global $INDEXOR_DBUSER, $INDEXOR_DBNAME, $INDEXOR_DBHOST, $INDEXOR_DBPASSWD;
	
			$this->debug = $debug;
			$this->table = "Log";	
			$this->db = new Database($debug);
			$this->db->connect($INDEXOR_DBHOST, $INDEXOR_DBUSER, $INDEXOR_DBPASSWD);
			$this->db->usedatabase($INDEXOR_DBNAME);
		}

		function store($searchfor, $results, $ip) {
			if(!$this->exists($searchfor, $ip)) {
				$hostname = @gethostbyaddr($ip);
				$this->db->query("LOCK TABLES $this->table WRITE");
				$this->db->query("insert into $this->table (SearchFor, Results, Ip, HostName) values(\"$searchfor\", \"$results\", \"$ip\", \"$hostname\")");
				$this->db->query("UNLOCK TABLES");
			}
		}

		function total_searches() {
			$array = $this->db->get_array($this->db->query("select count(*) as number from $this->table"));
			return $array[number];
		}

		function exists($s, $ip) {
			if(!$s) return 1;
			return $this->db->num_rows($this->db->query("select * from $this->table where SearchFor like '%$s%' and Ip=\"$ip\""));
		}

		function display(){
			if (!$this->resultid) {
				$veiwlogs = "select * from $this->table";
				$this->resultid = $this->db->query($veiwlogs);
			}
			$numresult = $this->db->num_rows($this->resultid);
				print "
						<TABLE border=0>
							<TR ALIGN=CENTER>
								<TD><U>IP</U></TD>
								<TD><U>Browser</U></TD>";
			for($step = 0; $step < $numresult; $step++){
				$res2 = $this->db->get_array($this->resultid);
				print "
					<TR>
						<TD>$res2[Ip]</TD>
						<TD>$res2[Browserinfo]</TD>
					</TR>";
			}
			print "
				</TABLE><BR><BR>
			";
		}
	}
}

?>
