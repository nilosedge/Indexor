#ifndef QueryNode_h_
#define QueryNode_h_

#include <iostream>
#include "Node.h"
#include <mysql/mysql.h>

using namespace std;

#define INSERT_QUERY 1
#define SELECT_QUERY 2
#define UPDATE_QUERY 3
#define DELETE_QUERY 4

class QueryNode : public Node {

	public:
		QueryNode(char *_Query, int _Type, bool _CanDelete);
		void Print();
		~QueryNode();

		volatile bool CanDelete;
		volatile bool Completed;
		volatile int Type;
		/*
			1 Insert
			2 select
			3 Update
			4 delete
		*/
		char Query[10000];
		MYSQL_RES *result;
		volatile unsigned long long Index;

};

#endif
