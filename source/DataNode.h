#ifndef DataNode_h_
#define DataNode_h_

#include "Node.h"
#include <iostream>
	

	class DataNode : public Node {

		public:
			DataNode(unsigned long long _data) : data(_data) {};
			DataNode(char *_worddata) : worddata(_worddata) {};
			DataNode(unsigned long long _data, char *_worddata) : data(_data), worddata(_worddata) {};
			void Print();
			~DataNode() { if(worddata) delete worddata; }
			unsigned long long data;
			char *worddata;

	};

#endif
