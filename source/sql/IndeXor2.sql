-- phpMyAdmin SQL Dump
-- version 2.6.2-pl1
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jul 15, 2005 at 11:41 AM
-- Server version: 4.0.24
-- PHP Version: 4.3.10-15
-- 
-- Database: `IndeXor2`
-- 

-- --------------------------------------------------------

DROP DATABASE if EXISTS `IndeXor2`;
CREATE DATABASE IF NOT EXISTS `IndeXor2`;

-- 
-- Table structure for table `Files`
-- 

DROP TABLE IF EXISTS `Files`;
CREATE TABLE IF NOT EXISTS `Files` (
  `Id` mediumint(9) NOT NULL auto_increment,
  `HostId` mediumint(9) NOT NULL default '0',
  `FilePath` varchar(255) NOT NULL default '',
  `FileName` varchar(255) NOT NULL default '',
  `Size` double NOT NULL default '0',
  `Updated` timestamp(14) NOT NULL,
  `Reliability` smallint(6) NOT NULL default '0',
  PRIMARY KEY  (`Id`),
  KEY `HostId` (`HostId`),
  KEY `FilePath` (`FilePath`),
  KEY `FileName` (`FileName`),
  KEY `Size` (`Size`),
  KEY `Reliability` (`Reliability`),
  KEY `Updated` (`Updated`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `Files`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `Hosts`
-- 

DROP TABLE IF EXISTS `Hosts`;
CREATE TABLE IF NOT EXISTS `Hosts` (
  `Id` mediumint(9) NOT NULL auto_increment,
  `Name` char(40) NOT NULL default '',
  `Available` smallint(6) NOT NULL default '0',
  `Reliability` smallint(6) NOT NULL default '0',
  `Ip` char(15) NOT NULL default '',
  PRIMARY KEY  (`Id`),
  KEY `Name` (`Name`),
  KEY `Available` (`Available`),
  KEY `Reliability` (`Reliability`),
  KEY `Ip` (`Ip`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `Hosts`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `Keywords`
-- 

DROP TABLE IF EXISTS `Keywords`;
CREATE TABLE IF NOT EXISTS `Keywords` (
  `FileId` mediumint(9) NOT NULL default '0',
  `KeyWord` varchar(128) NOT NULL default '',
  PRIMARY KEY  (`FileId`,`KeyWord`),
  KEY `FileId` (`FileId`),
  KEY `KeyWord` (`KeyWord`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `Keywords`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `Log`
-- 

DROP TABLE IF EXISTS `Log`;
CREATE TABLE IF NOT EXISTS `Log` (
  `Id` mediumint(9) NOT NULL auto_increment,
  `SearchFor` varchar(64) NOT NULL default '',
  `Results` mediumint(9) NOT NULL default '0',
  `Ip` varchar(15) NOT NULL default '',
  `HostName` tinytext NOT NULL,
  `Time` timestamp(14) NOT NULL,
  PRIMARY KEY  (`Id`),
  KEY `Id` (`Id`),
  KEY `SearchFor` (`SearchFor`),
  KEY `Ip` (`Ip`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `Log`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `Skipwords`
-- 

DROP TABLE IF EXISTS `Skipwords`;
CREATE TABLE IF NOT EXISTS `Skipwords` (
  `Word` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`Word`),
  KEY `Word` (`Word`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `Skipwords`
-- 

INSERT INTO `Skipwords` (`Word`) VALUES ('0');
INSERT INTO `Skipwords` (`Word`) VALUES ('1');
INSERT INTO `Skipwords` (`Word`) VALUES ('2');
INSERT INTO `Skipwords` (`Word`) VALUES ('3');
INSERT INTO `Skipwords` (`Word`) VALUES ('4');
INSERT INTO `Skipwords` (`Word`) VALUES ('5');
INSERT INTO `Skipwords` (`Word`) VALUES ('6');
INSERT INTO `Skipwords` (`Word`) VALUES ('7');
INSERT INTO `Skipwords` (`Word`) VALUES ('8');
INSERT INTO `Skipwords` (`Word`) VALUES ('9');
INSERT INTO `Skipwords` (`Word`) VALUES ('a');
INSERT INTO `Skipwords` (`Word`) VALUES ('all');
INSERT INTO `Skipwords` (`Word`) VALUES ('an');
INSERT INTO `Skipwords` (`Word`) VALUES ('and');
INSERT INTO `Skipwords` (`Word`) VALUES ('are');
INSERT INTO `Skipwords` (`Word`) VALUES ('as');
INSERT INTO `Skipwords` (`Word`) VALUES ('at');
INSERT INTO `Skipwords` (`Word`) VALUES ('b');
INSERT INTO `Skipwords` (`Word`) VALUES ('be');
INSERT INTO `Skipwords` (`Word`) VALUES ('but');
INSERT INTO `Skipwords` (`Word`) VALUES ('by');
INSERT INTO `Skipwords` (`Word`) VALUES ('c');
INSERT INTO `Skipwords` (`Word`) VALUES ('can');
INSERT INTO `Skipwords` (`Word`) VALUES ('d');
INSERT INTO `Skipwords` (`Word`) VALUES ('did');
INSERT INTO `Skipwords` (`Word`) VALUES ('do');
INSERT INTO `Skipwords` (`Word`) VALUES ('e');
INSERT INTO `Skipwords` (`Word`) VALUES ('each');
INSERT INTO `Skipwords` (`Word`) VALUES ('f');
INSERT INTO `Skipwords` (`Word`) VALUES ('find');
INSERT INTO `Skipwords` (`Word`) VALUES ('for');
INSERT INTO `Skipwords` (`Word`) VALUES ('from');
INSERT INTO `Skipwords` (`Word`) VALUES ('g');
INSERT INTO `Skipwords` (`Word`) VALUES ('h');
INSERT INTO `Skipwords` (`Word`) VALUES ('had');
INSERT INTO `Skipwords` (`Word`) VALUES ('has');
INSERT INTO `Skipwords` (`Word`) VALUES ('have');
INSERT INTO `Skipwords` (`Word`) VALUES ('he');
INSERT INTO `Skipwords` (`Word`) VALUES ('her');
INSERT INTO `Skipwords` (`Word`) VALUES ('him');
INSERT INTO `Skipwords` (`Word`) VALUES ('his');
INSERT INTO `Skipwords` (`Word`) VALUES ('how');
INSERT INTO `Skipwords` (`Word`) VALUES ('i');
INSERT INTO `Skipwords` (`Word`) VALUES ('if');
INSERT INTO `Skipwords` (`Word`) VALUES ('in');
INSERT INTO `Skipwords` (`Word`) VALUES ('into');
INSERT INTO `Skipwords` (`Word`) VALUES ('is');
INSERT INTO `Skipwords` (`Word`) VALUES ('it');
INSERT INTO `Skipwords` (`Word`) VALUES ('its');
INSERT INTO `Skipwords` (`Word`) VALUES ('j');
INSERT INTO `Skipwords` (`Word`) VALUES ('just');
INSERT INTO `Skipwords` (`Word`) VALUES ('k');
INSERT INTO `Skipwords` (`Word`) VALUES ('know');
INSERT INTO `Skipwords` (`Word`) VALUES ('l');
INSERT INTO `Skipwords` (`Word`) VALUES ('like');
INSERT INTO `Skipwords` (`Word`) VALUES ('long');
INSERT INTO `Skipwords` (`Word`) VALUES ('m');
INSERT INTO `Skipwords` (`Word`) VALUES ('made');
INSERT INTO `Skipwords` (`Word`) VALUES ('make');
INSERT INTO `Skipwords` (`Word`) VALUES ('may');
INSERT INTO `Skipwords` (`Word`) VALUES ('more');
INSERT INTO `Skipwords` (`Word`) VALUES ('most');
INSERT INTO `Skipwords` (`Word`) VALUES ('my');
INSERT INTO `Skipwords` (`Word`) VALUES ('n');
INSERT INTO `Skipwords` (`Word`) VALUES ('no');
INSERT INTO `Skipwords` (`Word`) VALUES ('not');
INSERT INTO `Skipwords` (`Word`) VALUES ('now');
INSERT INTO `Skipwords` (`Word`) VALUES ('o');
INSERT INTO `Skipwords` (`Word`) VALUES ('of');
INSERT INTO `Skipwords` (`Word`) VALUES ('on');
INSERT INTO `Skipwords` (`Word`) VALUES ('one');
INSERT INTO `Skipwords` (`Word`) VALUES ('only');
INSERT INTO `Skipwords` (`Word`) VALUES ('or');
INSERT INTO `Skipwords` (`Word`) VALUES ('p');
INSERT INTO `Skipwords` (`Word`) VALUES ('q');
INSERT INTO `Skipwords` (`Word`) VALUES ('r');
INSERT INTO `Skipwords` (`Word`) VALUES ('s');
INSERT INTO `Skipwords` (`Word`) VALUES ('said');
INSERT INTO `Skipwords` (`Word`) VALUES ('see');
INSERT INTO `Skipwords` (`Word`) VALUES ('she');
INSERT INTO `Skipwords` (`Word`) VALUES ('so');
INSERT INTO `Skipwords` (`Word`) VALUES ('some');
INSERT INTO `Skipwords` (`Word`) VALUES ('t');
INSERT INTO `Skipwords` (`Word`) VALUES ('than');
INSERT INTO `Skipwords` (`Word`) VALUES ('that');
INSERT INTO `Skipwords` (`Word`) VALUES ('the');
INSERT INTO `Skipwords` (`Word`) VALUES ('them');
INSERT INTO `Skipwords` (`Word`) VALUES ('then');
INSERT INTO `Skipwords` (`Word`) VALUES ('there');
INSERT INTO `Skipwords` (`Word`) VALUES ('these');
INSERT INTO `Skipwords` (`Word`) VALUES ('they');
INSERT INTO `Skipwords` (`Word`) VALUES ('this');
INSERT INTO `Skipwords` (`Word`) VALUES ('time');
INSERT INTO `Skipwords` (`Word`) VALUES ('to');
INSERT INTO `Skipwords` (`Word`) VALUES ('two');
INSERT INTO `Skipwords` (`Word`) VALUES ('u');
INSERT INTO `Skipwords` (`Word`) VALUES ('up');
INSERT INTO `Skipwords` (`Word`) VALUES ('use');
INSERT INTO `Skipwords` (`Word`) VALUES ('v');
INSERT INTO `Skipwords` (`Word`) VALUES ('very');
INSERT INTO `Skipwords` (`Word`) VALUES ('w');
INSERT INTO `Skipwords` (`Word`) VALUES ('was');
INSERT INTO `Skipwords` (`Word`) VALUES ('way');
INSERT INTO `Skipwords` (`Word`) VALUES ('we');
INSERT INTO `Skipwords` (`Word`) VALUES ('were');
INSERT INTO `Skipwords` (`Word`) VALUES ('what');
INSERT INTO `Skipwords` (`Word`) VALUES ('when');
INSERT INTO `Skipwords` (`Word`) VALUES ('where');
INSERT INTO `Skipwords` (`Word`) VALUES ('who');
INSERT INTO `Skipwords` (`Word`) VALUES ('will');
INSERT INTO `Skipwords` (`Word`) VALUES ('with');
INSERT INTO `Skipwords` (`Word`) VALUES ('x');
INSERT INTO `Skipwords` (`Word`) VALUES ('y');
INSERT INTO `Skipwords` (`Word`) VALUES ('you');
INSERT INTO `Skipwords` (`Word`) VALUES ('your');
INSERT INTO `Skipwords` (`Word`) VALUES ('z');
